<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    public function create()
    {
        return view('tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio'  => 'required'
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio']
        ]);

        return redirect('tambah');
    }

    public function index()
    {
        $cast = DB::Table('cast')->get();
        return view('tampil', ['cast' => $cast]);
    }
}

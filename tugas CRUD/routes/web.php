<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\castController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/main', function () {
    return view('template.main');
});


Route::get('/tambah',[castController::class,'create'])->name('tambah');
Route::post('/tambah_data',[castController::class,'store']);

Route::get('/tambah_data',[castController::class,'index']);


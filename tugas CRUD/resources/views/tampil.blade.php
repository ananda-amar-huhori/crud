@extends('template.main')
@section('konten')

<a href="tambah" class="btn btn primary btn-sm mb-3">tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">id</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">bio</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td>{{$value->bio}}</td>
            <td>
                <a href="" class="btn btn info btn-sm">Detil</a>
            </td>
        </tr>
        @empty
            <p>No users</p>
            <tr>
                <td>tidak ada data</td>
            </tr>
        @endforelse
    </tbody>
  </table>

@endsection
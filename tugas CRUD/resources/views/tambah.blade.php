@extends('template.main')
@section('konten')

<form action="/tambah_data" method="POST" style="padding: 20px">
    @csrf
    <div class="form-group">
      <label class="form-label">nama</label>
      <input name="nama" type="text" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label class="form-label">umur</label>
        <input name="umur" type="number" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>bio</label><br>
      <textarea name="bio" id="" cols="100" rows="10"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection